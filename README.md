# Crepuscolar_Rays
Implementation of the effects of Crepuscolar Rays based on the technique developed by Kenny Mitchell.
## Table of Contents  
- [About the Project](#1)  
  - [Built with](#2)
- [Usage](#3)

# About the Project <a name="1"/>
Empty Project.
# Built with <a name="2"/>

# Usage <a name="3"/>

# Authors
- [**Lorenzo Gianassi**](https://github.com/LorenzoGianassi)
- [**Francesco Gigli**](https://github.com/FrancescoGigli)
# Acknowledgments
Computer Graphics and 3D Project © Course held by Professor [Stefano Berretti](https://www.unifi.it/p-doc2-2019-0-A-2b333d293228-1.html) - Computer Engineering Master Degree @[University of Florence](https://www.unifi.it/changelang-eng.html)

